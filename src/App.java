import com.devcamp.s50.Task5640.InvoiceItem;

public class App {
    public static void main(String[] args) throws Exception {
        InvoiceItem invoiceItem1 = new InvoiceItem();
        InvoiceItem invoiceItem2 = new InvoiceItem("01NASU", "Car", 6, 1000);
        System.out.println(invoiceItem1.toString());
        System.out.println(invoiceItem2.toString());

        double total1 = invoiceItem1.getTotal();
        double total2 = invoiceItem2.getTotal();
        System.out.println("Total Price 1: " + total1);
        System.out.println("Total Price 2: " + total2);
    }
}
