package com.devcamp.s50.Task5640;

public class InvoiceItem {
    private String id = "none";
    private String decs = "none";
    private int qty = 0;
    private double unitPrice = 0.0;

    public InvoiceItem() {
        super();
    }

    public InvoiceItem(String id, String decs, int qty, double unitPrice) {
        this.id = id;
        this.decs = decs;
        this.qty = qty;
        this.unitPrice = unitPrice;
    }

    public String getId() {
        return this.id;
    }

    public String getDecs() {
        return this.decs;
    }

    public int getQty() {
        return this.qty;
    }

    public double getUnitPrice() {
        return this.unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public double getTotal() {
        return qty * unitPrice;
    }

    @Override
    public String toString() {
        return String.format("Invoice Item[id= %s, decs= %s, qty= %s, unit Price= %s]", id, decs, qty, unitPrice);
    }
}
